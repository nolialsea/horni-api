# Horni API

Loads the horni-ln model and serves it through REST endpoints  
Created as a base for other services

# Requirements

- git
- Python 3.7+
- A GPU with at least 6gb of VRAM and compatible with CUDA
- CUDA
- Download `gpt-neo-2.7B-horni-ln` model from either:
  - [MEGA](https://mega.nz/file/rQcWCTZR#tCx3Ztf_PMe6OtfgI95KweFT5fFTcMm7Nx9Jly_0wpg)
  - [Google Drive](https://drive.google.com/file/d/1M1JY459RBIgLghtWDRDXlD4Z5DAjjMwg/view)
  - Torrent: magnet:?xt=urn:btih:31d956ff4a248dcf914b1b7e474cbac02d70d6a4&dn=gtp-neo-horni
- Extract the model tar file into this project's folder
- Run those commands one by one in your shell to install Python dependencies
```
pip install git+https://github.com/finetuneanon/transformers@gpt-neo-dungeon-localattention2
pip install torch
pip install gdown
pip install flask
pip install flask_json
```
- Run `python ./main.py` to start the REST API server
