import datetime
import json

import torch
from flask import Flask, request
from flask_json import FlaskJSON
from transformers import GPTNeoForCausalLM, AutoTokenizer

print("Setting up model...")

model_name = "2.7B-horni-ln"

torch.cuda.empty_cache()
checkpoint = torch.load("gpt-neo-" + model_name + "/pytorch_model.bin", map_location="cpu")
for k in checkpoint.keys():
    checkpoint[k] = checkpoint[k].half()
model = GPTNeoForCausalLM.from_pretrained("gpt-neo-" + model_name).to(torch.float16).to("cuda").eval()
for k in list(checkpoint.keys()):
    del checkpoint[k]
del checkpoint

tokenizer = AutoTokenizer.from_pretrained("gpt2")
vocab = tokenizer.get_vocab()
vocab_keys = vocab.keys()

api = Flask(__name__)
FlaskJSON(api)


def find_keys(char, case_sensitive=False):
    keys = list()
    for k in vocab_keys:
        if case_sensitive:
            keep = k.find(char) != -1
        else:
            keep = k.lower().find(char.lower()) != -1

        if keep:
            keys.append(k)
    return keys


def append_bad_keys(bad_keys, prevent_square_brackets, prevent_curly_brackets, prevent_angle_brackets):
    if prevent_square_brackets:
        bad_keys.extend(find_keys("["))
        # bad_keys.extend(find_keys("]"))

    if prevent_angle_brackets:
        bad_keys.extend(find_keys("<"))
        bad_keys.extend(find_keys(">"))

    if prevent_curly_brackets:
        bad_keys.extend(find_keys("{"))
        # bad_keys.extend(find_keys("}"))


def crop_prompt(prompt, max_tokens=2048):
    context_tokens = tokenize(prompt)
    cropped_tokens = crop_tokens(context_tokens, max_tokens)
    return tokenizer.decode(cropped_tokens)


def crop_tokens(tokens, max_tokens=2048):
    if len(tokens) >= max_tokens:
        tokens = tokens[-max_tokens:]
    return tokens


def tokenize(prompt):
    tokens = tokenizer.encode(prompt, add_special_tokens=False)
    torch.cuda.empty_cache()
    return tokens


@api.route('/health_check', methods=['GET'])
def health_check():
    return "ok"


@api.route('/tokens', methods=['POST'])
def get_tokens():
    data = request.get_json(force=True)
    prompt = data['prompt']
    tokens = tokenize(prompt)
    return json.dumps(tokens, indent=2)


@api.route('/string-tokens', methods=['POST'])
def get_all_tokens_from_string():
    data = request.get_json(force=True)
    string = data['string']
    case_sensitive = bool(data['case_sensitive']) if 'case_sensitive' in data else False
    return json.dumps(find_keys(string, case_sensitive))


@api.route('/prompt', methods=['POST'])
def get_prompt():
    time_request_start = datetime.datetime.now()
    data = request.get_json(force=True)
    prompt = data['prompt']
    nb_answer = int(data['nb_answer']) if 'nb_answer' in data else 1
    number_generated_tokens = int(data['number_generated_tokens']) if 'number_generated_tokens' in data else 20
    temperature = float(data['temperature']) if 'temperature' in data else 0.8
    top_k = int(data['top_k']) if 'top_k' in data else 60
    top_p = float(data['top_p']) if 'top_p' in data else 0.9
    tail_free_sampling = float(data['tail_free_sampling']) if 'tail_free_sampling' in data else 0.95
    repetition_penalty = float(data['repetition_penalty']) if 'repetition_penalty' in data else 2.0
    repetition_penalty_range = int(data['repetition_penalty_range']) if 'repetition_penalty_range' in data else 512
    repetition_penalty_slope = float(data['repetition_penalty_slope']) if 'repetition_penalty_slope' in data else 3.33

    enable_tfs = bool(data['enable_tfs']) if 'enable_tfs' in data else False
    enable_top_k = bool(data['enable_top_k']) if 'enable_top_k' in data else True
    enable_top_p = bool(data['enable_top_p']) if 'enable_top_p' in data else True

    prevent_square_brackets = bool(data['prevent_square_brackets']) if 'prevent_square_brackets' in data else False
    prevent_angle_brackets = bool(data['prevent_angle_brackets']) if 'prevent_angle_brackets' in data else False
    prevent_curly_brackets = bool(data['prevent_curly_brackets']) if 'prevent_curly_brackets' in data else False

    banned_ids = data['banned_token_indexes'] if 'banned_token_indexes' in data else list()
    banned_strings = data['banned_strings'] if 'banned_strings' in data else list()

    if not enable_tfs:
        tail_free_sampling = None
    if not enable_top_k:
        top_k = None
    if not enable_top_p:
        top_p = None

    bad_keys = list()
    bad_keys_final = list()
    bad_words_ids = list()
    append_bad_keys(bad_keys, prevent_square_brackets, prevent_curly_brackets, prevent_angle_brackets)

    for key in banned_strings:
        bad_keys.extend(find_keys(key))

    for key in bad_keys:
        if key == "<|endoftext|>" or key in bad_keys_final:
            continue
        bad_id = vocab[key]
        bad_words_ids.append([bad_id])
        bad_keys_final.append(key)

    if banned_ids is not None:
        for banned_id in banned_ids:
            if banned_id is not None:
                bad_words_ids.append([banned_id])

    if len(bad_words_ids) < 1:
        bad_words_ids = None

    prompt = crop_prompt(prompt, 2048 - number_generated_tokens)
    ids = tokenizer(prompt, return_tensors="pt").input_ids.to("cpu")
    n_ids = ids.shape[1]
    if n_ids < 1:
        n_ids = 1
        ids = torch.tensor([[tokenizer.eos_token_id]])
    max_length = min(2048, n_ids + number_generated_tokens)
    prompt_tokens = tokenize(prompt)
    ret = []
    for i in range(nb_answer):
        time_start = datetime.datetime.now()
        basic_output = model.generate(
            ids.long().cuda(),
            do_sample=True,
            min_length=max_length,
            max_length=max_length,
            temperature=temperature,
            tfs=tail_free_sampling,
            top_k=top_k,
            top_p=top_p,
            repetition_penalty=repetition_penalty,
            repetition_penalty_range=repetition_penalty_range,
            repetition_penalty_slope=repetition_penalty_slope,
            use_cache=True,
            bad_words_ids=bad_words_ids,
            pad_token_id=tokenizer.eos_token_id
        ).long().to("cpu")
        result = tokenizer.decode(basic_output[0])
        string_result = result[len(prompt):]
        tokens = tokenize(string_result)
        torch.cuda.empty_cache()
        time_end = datetime.datetime.now()
        elapsed_time = time_end - time_start
        obj = {
            "content": string_result,
            "tokens": tokens,
            "elapsed_time": elapsed_time.total_seconds() * 1000
        }
        ret.append(obj)
    time_request_end = datetime.datetime.now()
    elapsed_time_total = time_request_end - time_request_start
    result = {
        "prompt": {
            "content": prompt,
            "tokens": prompt_tokens,
            "bad_token_ids": bad_words_ids
        },
        "total_elapsed_time": elapsed_time_total.total_seconds() * 1000,
        "results": ret
    }
    return json.dumps(result)


if __name__ == '__main__':
    api.run()
